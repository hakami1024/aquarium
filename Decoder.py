from Models.Aquarium import Aquarium
from Models.Fish import Fish, Gender
from Models.FishType import FishType
from Models.User import User

__author__ = 'hakami'

class Decoder:

    def decode(self, o):
        return self.decode_user(o)

    def decode_user(self, user):
        result = User(user["login"], user["password"])
        for aquarium in user["aquariums"]:
            result.aquariums.append(self.decode_aquarium(aquarium))
        return result

    def decode_aquarium(self, aquarium):
        result = Aquarium(aquarium["name"], float(aquarium["water_amount"]))
        for fish in aquarium["fishes"]:
            result.add_fish(self.decode_fish(fish, result))

        return result

    def decode_fish(self, fish, aquarium):
        result = Fish(FishType.find_type_by_name(fish["fish_type"]),
                      aquarium, int(fish["age"]), Gender.get_from_string(fish["gender"]))
        return result
