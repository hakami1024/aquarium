from django.contrib import admin

# Register your models here.
from .models import *


class AquariumAdmin(admin.ModelAdmin):
    list_display = ["name", "water_amount", "user"]


class FishTypeAdmin(admin.ModelAdmin):
    list_display = ["name"]


class FishAdmin(admin.ModelAdmin):
    list_display = ["id", "fish_type", "gender", "age", "lifetime", "aquarium"]

admin.site.register(Aquarium, AquariumAdmin)
admin.site.register(FishType, FishTypeAdmin)
admin.site.register(Fish, FishAdmin)
