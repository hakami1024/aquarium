from django import forms

from .models import Fish, FishType, Aquarium


# class DeleteAquariumForm(forms.Form):
#     aquarium_id = forms.IntegerField(label='Your name', max_length=100)

class AddFishForm(forms.ModelForm):
    fish_type = forms.ModelChoiceField(queryset=FishType.objects.all(), empty_label=None)
    class Meta:
        model=Fish
        fields = ["fish_type", "age", "gender"]

class AddAquariumForm(forms.ModelForm):
    class Meta:
        model=Aquarium
        fields = ["name", "water_amount"]
