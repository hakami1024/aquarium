from django.conf.urls import url
from django.views.generic import RedirectView

from . import views
from django.conf.urls import handler403

handler403 = 'aquariums.views.permission_denied'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<aquarium_id>[0-9]+)', views.detail, name='detail'),
    url(r'^fishtypes', views.fishtypes),
    url(r'logout', 'django.contrib.auth.views.logout',
                          {'next_page': '/accounts/login/'}),
    url(r'runlife', views.run_iteration),
    url(r'addfish', views.add_fish),
    url(r'movefish', views.move_fish),
    url(r'addaquarium', views.add_aquarium),
    url(r'editaquarium/(?P<aquarium_id>[0-9]+)/save', views.edit_aquarium_save),
    url(r'editaquarium/(?P<aquarium_id>[0-9]+)', views.edit_aquarium),
    url(r'^deleteaquarium', views.delete_aquarium),
    url(r'^deletefish', views.delete_fish),
    url(r'^.*$', RedirectView.as_view(url='/', permanent=False), name='index'),
]