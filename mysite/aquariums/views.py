from django.contrib.auth.decorators import login_required
from django.forms import model_to_dict
from django.http import HttpResponseForbidden
from django.shortcuts import render, get_object_or_404, render_to_response, redirect
from django.template import RequestContext, loader

from .forms import AddFishForm, AddAquariumForm
from .models import Aquarium, Fish, FishType


@login_required
def index(request):
    aquarium_list = Aquarium.objects.order_by('-name').filter(user=request.user)
    context = {'aquarium_list': aquarium_list, "add_aquarium_form": AddAquariumForm()}

    return render(request, "aquariums/index.html", context)


@login_required
def detail(request, aquarium_id):
    aquarium = get_object_or_404(Aquarium, pk=aquarium_id)

    if aquarium.user != request.user:
        return HttpResponseForbidden(loader.render_to_string("403.html"))

    aquarium_list = Aquarium.objects.filter(user=request.user).exclude(id=aquarium.id)

    fishes = Fish.objects.filter(aquarium=aquarium).order_by('-age', 'id')
    statistics = {"total": str(Fish.total_count(aquarium)), "females": str(Fish.females_count(aquarium)),
                  "fries": str(Fish.fries_count(aquarium))}
    fishtypes = FishType.objects.all()
    stat_array = []
    for type in fishtypes:
        typestat = {}
        typestat["name"] = type.name
        typestat["total"] = str(Fish.total_count(aquarium, type))
        typestat["females"] = str(Fish.females_count(aquarium, type))
        typestat["fries"] = str(Fish.fries_count(aquarium, type))
        stat_array.append(typestat)

    statistics["types"] = stat_array
    return render(request, 'aquariums/detail.html', {'aquarium': aquarium, 'fishes': fishes,
                                                     'aquariums_list': aquarium_list, "add_fish_form": AddFishForm(), "statistics": statistics})

@login_required
def fishtypes(request):
    fishtypes = FishType.objects.all()
    stat_array = []
    for type in fishtypes:
        typestat = {}
        typestat["name"] = type.name
        typestat["total"] = str(Fish.total_count(type=type))
        typestat["females"] = str(Fish.females_count(type=type))
        typestat["fries"] = str(Fish.fries_count(type=type))
        stat_array.append(typestat)

    return render(request, 'aquariums/fishtypes.html', {"fishtypes_stat": zip(fishtypes, stat_array), "typestatistics": stat_array})


def permission_denied(request):
    response = render_to_response(
        '403.html',
        context_instance=RequestContext(request)
    )

    response.status_code = 403

    return response

@login_required
def delete_aquarium(request):
    id = request.POST['aquarium_id']
    aquarium = get_object_or_404(Aquarium, pk=id)

    if aquarium.user != request.user:
        return HttpResponseForbidden(loader.render_to_string("403.html"))

    aquarium.delete()
    return index(request)

@login_required
def delete_fish(request):
    id = request.POST['fish_id']
    fish = get_object_or_404(Fish, pk=id)
    aquarium_id = fish.aquarium.id

    if fish.aquarium.user != request.user:
        return HttpResponseForbidden(loader.render_to_string("403.html"))

    fish.delete()
    return redirect("aquariums/" + str(aquarium_id)+"/")

@login_required
def move_fish(request):
    id = request.POST['fish_id']
    fish = get_object_or_404(Fish, pk=id)
    old_aquarium = fish.aquarium

    if not 'new_aquarium_id' in request.POST.keys():
        return redirect("aquariums/"+str(old_aquarium.id)+"/")

    new_aq_id = request.POST['new_aquarium_id']
    new_aquarium = get_object_or_404(Aquarium, pk=new_aq_id)

    if fish.aquarium.user != request.user or new_aquarium.user != request.user:
        return HttpResponseForbidden(loader.render_to_string("403.html"))

    fish.aquarium = new_aquarium
    fish.save()

    return redirect("aquariums/"+str(old_aquarium.id)+"/")

@login_required
def add_fish(request):
    fish = AddFishForm(request.POST).save(commit=False)
    aquarium_id = request.POST["aquarium"]
    aquarium = get_object_or_404(Aquarium, id=aquarium_id)
    fish.aquarium = aquarium
    fish.set_lifetime()
    fish.save()
    # data = serializers.serialize('json', [fish,])
    # struct = json.loads(data)
    # data = json.dumps(struct[0])
    redir = "aquariums/"+str(aquarium.id)+"/"
    return redirect(redir)
    # return HttpResponse(data, content_type='application/json')

@login_required
def add_aquarium(request):
    aquarium = AddAquariumForm(request.POST).save(commit=False)
    aquarium.user = request.user
    aquarium.save()
    return redirect("aquariums/")

@login_required
def run_iteration(request):
    aquarium_id = request.POST["aquarium_id"]
    aquarium = get_object_or_404(Aquarium, id=aquarium_id)
    iterations = int(request.POST["iterations"])
    for i in range(iterations):
        update_aquarium(aquarium)

    aquarium.iteration += iterations
    aquarium.save()
    return redirect("/aquariums")

def update_aquarium(aquarium):
    fishes = Fish.objects.filter(aquarium=aquarium)
    for fish in fishes:
        if not fish.is_alife():
            fish.delete()
        else:
            fish.age += 1
            fish.spawn()
            fish.save()

@login_required
def edit_aquarium(request, aquarium_id):
    aquarium = get_object_or_404(Aquarium, id=aquarium_id)
    if aquarium.user != request.user:
        return HttpResponseForbidden(loader.render_to_string("403.html"))

    context = {"edit_aquarium_form": AddAquariumForm(instance=aquarium), "aquarium":aquarium}
    return render(request, "aquariums/edit_aquarium.html", context)

@login_required
def edit_aquarium_save(request, aquarium_id):
    aquarium = get_object_or_404(Aquarium, id=aquarium_id)
    clean = "clean" in request.POST.keys()

    aquarium = AddAquariumForm(request.POST, instance=aquarium).save(commit=(not clean))
    if aquarium.user != request.user:
        return HttpResponseForbidden(loader.render_to_string("403.html"))
    if clean:
        aquarium.iteration = 0
        Fish.objects.filter(aquarium=aquarium).delete()
        aquarium.save()
    return redirect("/aquariums")



