from random import Random

from django.contrib.auth.models import User
from django.db import models
from django.db.models import F


class Aquarium(models.Model):
    name = models.CharField(max_length=200)
    water_amount = models.IntegerField(default=0)
    user = models.ForeignKey(User, default=None)
    iteration = models.IntegerField(default=0)



    def __str__(self):
        return self.name + ' (' + str(self.water_amount) + ' liters )'

    def _filled_percent(self):
        fishes = Fish.objects.filter(aquarium=self)
        used = 0.0
        for fish in fishes:
            used += fish.fish_type.water_amount
        return used/float(self.water_amount)

    filled_percent = property(_filled_percent)


class FishType(models.Model):
    name = models.CharField(max_length=200)
    max_lifetime = models.IntegerField(default=0)
    water_amount = models.FloatField(default=1.0)
    fry_period = models.IntegerField(default=0)
    spawning_period = models.IntegerField(default=0)
    max_fries_count = models.IntegerField(default=0)
    males_percent = models.FloatField(default=0.5)
    fry_die_period = models.IntegerField(default=0)
    fry_deaths_percent = models.FloatField(default=0.0)
    image_url = models.CharField(max_length=1000, default="")

    def __str__(self):
        return self.name


class Fish(models.Model):
    MALE = 0
    FEMALE = 1
    random = Random()


    fish_type = models.ForeignKey(FishType)
    age = models.IntegerField(default=0)
    lifetime = models.IntegerField(default=0)
    gender = models.IntegerField(default=0, choices=((MALE, 'MALE'), (FEMALE, 'FEMALE')))
    aquarium = models.ForeignKey(Aquarium, default=None)

    def __str__(self):
        return str(self.fish_type) + '_' + str(self.id)

    def is_alife(self):
        return self.age < self.lifetime

    def set_lifetime(self):
        type = self.fish_type
        if self.age < type.fry_die_period:
            self.lifetime = Fish.random.randint(0, type.fry_die_period)
            survived_fry = type.fry_deaths_percent*100 > Fish.random.randint(0, 100)
            if survived_fry:
                 self.lifetime = Fish.random.randint(type.fry_die_period, type.max_lifetime)
        else:
            self.lifetime = Fish.random.randint(self.age, type.max_lifetime)

    def spawn(self):
        if self.gender == Fish.MALE \
            or self.age <= self.fish_type.fry_period \
            or self.age % self.fish_type.spawning_period != 0:
            return
        count = Fish.random.randint(0, self.fish_type.max_fries_count)
        for i in range(0, count):
            gender = Fish.FEMALE if Random().random() >= self.fish_type.males_percent else Fish.MALE
            fish = Fish.objects.create(fish_type=self.fish_type, age=0, lifetime = 0,
                        gender = gender, aquarium=self.aquarium)
            fish.set_lifetime()
            fish.save()

    @staticmethod
    def total_count(aquarium=None, type=None):
        query = Fish.objects
        if aquarium:
            query = query.filter(aquarium=aquarium)
        if type:
            query = query.filter(fish_type = type)
        return query.count()

    @staticmethod
    def females_count(aquarium=None, type=None):
        query = Fish.objects
        if aquarium:
            query = query.filter(aquarium=aquarium)
        if type:
            query = query.filter(fish_type = type)

        return query.filter(gender=Fish.FEMALE).filter(age__gte=F("fish_type__fry_period")).count()

    @staticmethod
    def fries_count(aquarium=None, type=None):
        query = Fish.objects
        if aquarium:
            query=query.filter(aquarium=aquarium)

        if type:
            query = query.filter(fish_type = type)

        return query.filter(age__lt=F("fish_type__fry_period")).count()
