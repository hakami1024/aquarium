from cmd import Cmd
import getopt
import json
import os
from os.path import expanduser
import traceback
import atexit
# import jsonpickle
import readline
from Decoder import Decoder
from Encoder import Encoder
from Models.Fish import Gender, Fish
from Models.FishType import FishType
from Models.User import User, InvalidPasswordException, InvalidLoginException

__author__ = 'hakami'


def get_base_dir():
    return expanduser("~") + "/.aquarium/"


def get_history_file():
    if not os.path.exists(get_base_dir()):
        os.mkdir(get_base_dir())
    if not os.path.exists(get_base_dir()+"history.log"):
        os.mknod(get_base_dir()+"history.log")
    return get_base_dir()+"history.log"


def get_users_dir(user):
    return get_base_dir() + user.login + "/"


def on_exit():
    readline.write_history_file(get_history_file())


def delete_dir(dir):
    for root, dirs, files in os.walk(dir, topdown=False):
        for name in files:
            os.remove(os.path.join(root, name))
        for name in dirs:
            os.rmdir(os.path.join(root, name))


def init_users():
    if not os.path.exists(get_base_dir()):
        return

    for directory in [x[0] for x in os.walk(get_base_dir())]:
        if directory == get_base_dir():
            continue

        if not os.path.exists(directory+"/config"):
            os.rmdir(directory)
            continue

        f = open(directory+"/config", "r")
        data = "".join(f.readlines())
        f.close()
        if not data:
            continue
        # user = jsonpickle.decode(data, keys=True)
        # print(data)
        # print(json.loads(data))
        user = Decoder().decode(json.loads(data))
        User.users[user.login] = user.password


class Commands(Cmd):
    prompt = 'aquarium> '

    def __init__(self):
        super().__init__()
        self.curUser = None

    def preloop(self):
        readline.read_history_file(get_history_file())
        readline.set_history_length(100)

    def do_fishtypes(self, s):
        """
        Prints available fishtypes. Does not require arguments
        """
        for fishtype in FishType.all_types:
            print(str(fishtype))

    def do_exit(self, s):
        """
        Exits program
        """
        return True

    # region--------------------------User adding/deleting-------------------------------
    def do_signin(self, s):
        """
        Enter login and password. New user will be created, if login is not in the base.
        Example:
        signin username password
        """
        login, password = getopt.getopt(s.split(), "")[1]
        try:
            self.curUser = User(login, password)
            if not self.is_aquarium_app_configured():
                self.save_user_config()
            else:
                self.load_user_config()

            self.prompt = "aquarium({0})> ".format(login)
        except Exception as e:
            if isinstance(e, InvalidPasswordException):
                print("User with this login exists, but haves another password")
            elif isinstance(e, InvalidLoginException):
                print("Invalid login. Login should begin with letter")
            else:
                traceback.print_tb(e)

    def do_logout(self, s):
        """
        Logouting
        """
        self.curUser = None
        self.prompt = "aquarium> "

    def do_signout(self, s):
        """
        Logout and remove current user from database
        """
        if self.curUser is None:
            print("Signin first.")
            return

        delete_dir(get_users_dir(self.curUser))
        User.users.pop(self.curUser.login)
        self.do_logout("")
    # endregion

    # region------------------------User's methods--------------------------------
    def do_aquariums(self, s):
        """
        Call without args to show user's aquariums list.
        Call with name and water amount (in liters) in the args to add aquarium:
        aquarium aquariumName 10
        """

        if self.curUser is None:
            print("Signin first.")
            return

        if not s:
            self.print_aquariums()
        else:
            data = s.split()
            if len(data) != 2:
                print("Invalid arguments count. Check out: help aquariums")
                return

            name, water_amount = data

            try:
                if self.curUser.add_aquarium(name, float(water_amount)):
                    self.save_user_config()
                    print("Created.")
                else:
                    print("Aquarium with name '"+name+"' already exists.")
            except ValueError:
                print("Error. Please, pass the water amount number as the second parameter.")

    def do_statistics(self, name):
        """
        Prints statistics for aquarium, given as the parameter.
        Example: statistics MyAquarium
        """
        if not name:
            print("Use with aquarium name.")
            return

        aquarium = self.curUser.find_aquarium(name)
        if aquarium is None:
            print("Aquarium with name '"+name+"' not found.")
            return

        delimiter = "-"*80

        maininfo = aquarium.get_main_info()
        for key in maininfo:
            print(key + str(maininfo[key]))

        statistics = aquarium.get_statistics()
        for key in statistics:
            print()
            print(delimiter)
            print(key)
            print(delimiter)
            for key1 in statistics[key]:
                print(key1 + str(statistics[key][key1]))

    def do_addfish(self, s):
        """
        Example: addfish MyAquarium FishType [age_in_months] [gender]
        Add fish to passed aquarium. FishType must be one of printed by fishtypes command.
        age_in_months must be a number, default is 0.
        gender must be MALE or FEMALE. Default is randomly given according to FishType statistics.
        """
        if self.curUser is None:
            print("Signin first.")
            return

        args = s.split()

        if len(args) < 2:
            print("Error. Not all arguments given.")
            return

        args += [None, None]

        aquarium_name, fishtype_name, age_str, gender_str = args[:4]
        aquarium = self.curUser.find_aquarium(aquarium_name)
        if not aquarium:
            print("Error. Incorrect aquarium name.")
            return

        fishtype = FishType.find_type_by_name(fishtype_name)
        if not fishtype:
            print("Error. Incorrect fishtype. You can checkout fish types by fishtypes command.")
            return

        age = 0 if not age_str else str(age_str)
        gender = None if not gender_str else Gender.get_from_string(gender_str)

        fish = Fish(fishtype, aquarium, age, gender)
        aquarium.add_fish(fish)
        self.save_user_config()

        print("Successfully added.")

    def do_newmonth(self, s):
        """
        Simulates one more month of aquariums' life.
        """
        if self.curUser is None:
            print("Signin first.")
            return

        assert self.is_aquarium_app_configured()

        f = open(get_users_dir(self.curUser) + "config", "r")
        data = "".join(f.readlines())
        f.close()

        if not data:
            os.rmdir(get_users_dir(self.curUser))
        else:
            self.curUser = Decoder().decode(json.loads(data))


    def logined(self):
        return self.curUser is None

    def print_aquariums(self):
        assert self.curUser is not None

        if not self.curUser.aquariums:
            print("No aquariums.")
            return

        for aquarium in self.curUser.aquariums:
            info = aquarium.get_main_info()
            for key in info:
                print(key + str(info[key]))
            print("-----------------------")

    def emptyline(self):
        pass

init_users()
atexit.register(on_exit)
Commands().cmdloop()
