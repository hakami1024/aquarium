from Models.Aquarium import Aquarium

__author__ = 'hakami'


class User:

    users = {}

    def __init__(self, login, password):
        if not login or login[0].isdigit():
            raise InvalidLoginException

        if login in User.users.keys():
            if password != User.users[login]:
                raise InvalidPasswordException
        else:
            User.users[login] = password

        self.aquariums = []
        self.login = login
        self.password = password

    def add_aquarium(self, name, water_amount):
        aquarium = Aquarium(name, water_amount)
        if aquarium not in self.aquariums:
            self.aquariums.append(Aquarium(name, water_amount))
            return True
        else:
            return False

    def find_aquarium(self, name):
        for aquarium in self.aquariums:
            if aquarium.name == name:
                return aquarium
        return None


class InvalidLoginException(Exception):
    def __init__(self):
        super(InvalidLoginException, self).__init__("Invalid login")


class InvalidPasswordException(Exception):
    def __init__(self):
        super(InvalidPasswordException, self).__init__("User already exists")