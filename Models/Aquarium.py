from collections import OrderedDict

from Models.Fish import Gender

__author__ = 'hakami'


class Aquarium:
    def __init__(self, name, water_amount):
        self.name = name
        self.water_amount = water_amount
        self.fishes = {}

    def __iadd__(self, fish):
        self.add_fish(fish)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.name == other.name
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def get_main_info(self):
        return OrderedDict((("Name: ", self.name ),
                            ("Water amount: ", self.water_amount),
                            ("Filled in percent: ", self.filled_percent() * 100)))

    def get_statistics(self):
        total = list(self.iter_fishes())
        maininf = self.generate_statistics_for_list(total)
        answer = OrderedDict((("Main info: ", maininf),))
        for fishtype in self.fishes.keys():
            answer.update(((fishtype.name, (self.generate_statistics_for_list(self.fishes[fishtype]))),))
        return answer

    def generate_statistics_for_list(self, total):
        total_count = len(total)
        alive = list(filter(lambda fish: fish.alive(), total))
        alive_count = len(alive)

        alive_adults = list(filter(lambda fish: fish.age > fish.fish_type.fry_period, alive))
        alive_adults_count = len(alive_adults)
        alive_fries_count = alive_count - alive_adults_count
        alive_newborns_count = len(list(filter(lambda fish: fish.age == 0, alive)))
        alive_males_count = len(list(filter(lambda fish: fish.gender == Gender.MALE, alive_adults)))
        alive_females_count = alive_adults_count - alive_males_count

        dead = filter(lambda fish: not fish.alive(), total)
        dead_count = total_count - alive_count
        dead_adults = len(list(filter(lambda fish: fish.age > fish.fish_type.fry_period, dead)))
        dead_fries = dead_count - dead_adults

        return OrderedDict((("Fishes total: ", total_count),
                              ("--Dead fishes total: ", dead_count),
                              ("----Fries: ", dead_fries),
                              ("----Adults: ", dead_adults),
                              ("--Alive fishes total: ", alive_count),
                              ("----Fries total: ", alive_fries_count),
                              ("------Newborns: ", alive_newborns_count),
                              ("----Alive males: ", alive_males_count),
                              ("----Alive females: ", alive_females_count)))

    def filled_percent(self):
        fish_space = 0.0
        for fishtype_list in self.fishes.values():
            for fish in fishtype_list:
                if fish.alive() and fish.age > fish.fish_type.fry_period:
                    fish_space += fish.fish_type.water_amount

        return fish_space / self.water_amount

    def add_fish(self, fish):
        if fish.fish_type not in self.fishes.keys():
            self.fishes[fish.fish_type] = [fish]
        else:
            self.fishes[fish.fish_type] += [fish]

    def iter_fishes(self):
        for fishlist in self.fishes.values():
            for fish in fishlist:
                yield fish

    def update_state(self):

        for fishlist in self.fishes.values():
            for fish in list(fishlist):
                if not fish.alive():
                    fishlist.remove(fish)

        for ftype, flist in list(self.fishes.items()):
            if not flist:
                self.fishes.pop(ftype)

        for fish in self.iter_fishes():
            if fish.alive():
                fish.age += 1
                fish.spawn()
