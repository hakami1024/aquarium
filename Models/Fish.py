from enum import Enum
from random import Random

__author__ = 'hakami'


class Gender(Enum):
    MALE = 1
    FEMALE = 2

    @staticmethod
    def get_from_string(str):
        str = str.upper()
        if str == "MALE":
            return Gender.MALE
        if str == "FEMALE":
            return Gender.FEMALE
        return None

class Fish:

    __random = Random()

    def __init__(self, fish_type, aquarium, age=0, gender=None):
        self.fish_type = fish_type
        self.aquarium = aquarium
        self.age = age
        Fish.calc_lifetime(self)
        self.gender = gender if gender is not None else Fish.calc_gender(self)

    def spawn(self):
        if self.gender == Gender.MALE \
                or self.age <= self.fish_type.fry_period \
                or self.age % self.fish_type.spawning_period != 0:
            return

        count = Fish.__random.randint(0, self.fish_type.max_fries_count)
        for i in range(0, count):
            self.aquarium.add_fish(Fish(self.fish_type, self.aquarium))

    def alive(self):
        return self.age < self.__life_length

    @staticmethod
    def calc_gender(self):
        return Gender.FEMALE if Random().random() >= self.fish_type.males_percent else Gender.MALE

    @staticmethod
    def calc_lifetime(fish):
        if hasattr(fish, "__life_length"):
            return

        type = fish.fish_type
        fish.__life_length = Fish.__random.randint(0, type.fry_die_period)

        survived_fry = type.fry_deaths_percent*100 > Fish.__random.randint(0, 100)
        if survived_fry:
            fish.__life_length += Fish.__random.randint(type.fry_die_period, type.max_lifetime)


