__author__ = 'hakami'

class FishType:

    all_types = []

    # time is in months
    def __init__(self, name, max_lifetime, water_amount, fry_period, spawning_period, max_fries_count, males_percent,
                 fry_die_period, fry_deaths_percent):
        self.name = name
        self.max_lifetime = max_lifetime
        self.water_amount = water_amount
        self.fry_period = fry_period
        self.spawning_period = spawning_period
        self.max_fries_count = max_fries_count
        self.males_percent = males_percent
        self.fry_die_period = fry_die_period
        self.fry_deaths_percent = fry_deaths_percent
        FishType.all_types += [self]

    def __str__(self):
        return ("Name: {0}\n"+"-"*80+"\nMax lifetime: {1}\nLiters for one fish: {2}\n"
               + "Fry's growth period: {3}\nSpawning period: {4}\nMax fries count in litter: {5}\nFemales percent: {6}\n"
                + "Fry's dieing period: {7}\nDeath percent for fries: {8}"+"\n"+"="*80)\
                .format(self.name, self.max_lifetime, self.water_amount, self.fry_period, self.spawning_period,
                        self.max_fries_count, (1-self.males_percent)*100, self.fry_die_period, self.fry_deaths_percent)

    @staticmethod
    def find_type_by_name(name):
        for type in FishType.all_types:
            if type.name == name:
                return type
        return None


Guppie = FishType("Guppie", 3*12, 2.0, 7, 6, 35, 0.5, 2, 0.5)
Swordtail = FishType("Swordtail", 5*12, 3.0, 12, 6, 50, 0.5, 3, 0.5)


