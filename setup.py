__author__ = 'hakami'

from setuptools import setup, find_packages
setup(
    name = "Aquarium",
    version = "0.1",
    packages = find_packages(),
    scripts = ['aquarium_cmd.py'],

    # Project uses reStructuredText, so ensure that the docutils get
    # installed or upgraded on the target machine
    install_requires = ['docutils>=0.3'],

    # metadata for upload to PyPI
    author = "hakami",
    author_email = "Ahlb@yandex.ru",
    description = "This is an aquarium simulation",
    license = "PSF",
    keywords = "aquarium"
    # could also include long_description, download_url, classifiers, etc.
)

