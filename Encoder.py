import json
from Models.Aquarium import Aquarium
from Models.Fish import Fish, Gender
from Models.User import User

__author__ = 'hakami'

class Encoder(json.JSONEncoder):

     def default(self, obj):
         if isinstance(obj, User):
             return {"login": obj.login, "password": obj.password,
                     "aquariums":list([self.default(aquarium) for aquarium in obj.aquariums])}
         if isinstance(obj, Aquarium):
             return {"name": obj.name, "water_amount": str(obj.water_amount),
                     "fishes":list([self.default(fish) for fish in obj.iter_fishes()])}
         if isinstance(obj, Fish):
             return {"fish_type": obj.fish_type.name, "aquarium": obj.aquarium.name,
                     "age":str(obj.age), "gender":"MALE" if obj.gender == Gender.MALE else "FEMALE"}

         # Let the base class default method raise the TypeError
         return json.JSONEncoder.default(self, obj)
